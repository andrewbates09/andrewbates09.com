# CV jekyll theme

Free theme for the creation of your CV. View demo [here](https://jekyll-cv.stavrospanakakis.com/).

## Features
- Lightweight
- Free & Open Source
- Mobile Responsive
- Dark Mode Support


## Preview
![Preview](./preview.png)

## Installation
- modified for GitLab Pages.

Go to https://username.github.io/jekyll-cv/

## Contributing
- Feel free to open issues and create pull requests for new features or skins

## License
- [MIT](./LICENSE)
